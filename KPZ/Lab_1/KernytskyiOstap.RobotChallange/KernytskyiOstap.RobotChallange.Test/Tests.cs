﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace KernytskyiOstap.RobotChallange.Test
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestDistance()
        {
            var p1 = new Position(10, 2);
            var p2 = new Position(5, 8);
            var algorithm = new KernytskyiOstapAlgorithm();
            Assert.AreEqual(algorithm.FindDistance(p1, p2), 61);
        }

        [TestMethod]
        public void TestMoveCommand1()
        {
            var map = new Map();
            var stationPos = new Position(10, 20);
            map.Stations.Add(new EnergyStation() {Energy = 1000, Position = stationPos, RecoveryRate = 50});
            var owner = new Owner() {Name = "Ostap"};
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, Owner = owner, Position = new Position(14, 10)}};
            var algorithm = new KernytskyiOstapAlgorithm();
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand) command).NewPosition, new Position(13, 17));
        }

        [TestMethod]
        public void TestNearestSpot()
        {
            var map = new Map();
            var stationPos = new Position(50, 40);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 50 });
            var owner = new Owner() { Name = "Ostap" };
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, Owner = owner, Position = new Position(1, 1)}};
            var algorithm = new KernytskyiOstapAlgorithm();
            algorithm.updateOnce(map);
            var nearestSpot = algorithm.FindNearestSpot(robots[0].Position, algorithm.myMap1);
            Assert.AreEqual(nearestSpot, new Position(47, 37));
        }

        [TestMethod]
        public void TestMoveCommand2()
        {
            var map = new Map();
            var stationPos = new Position(10, 20);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 50 });
            var owner = new Owner() { Name = "Ostap" };
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, Owner = owner, Position = new Position(14, 10)}};
            var algorithm = new KernytskyiOstapAlgorithm();
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, new Position(13, 17));
        }

        [TestMethod]
        public void TestCollect()
        {
            var map = new Map();
            var stationPos = new Position(10, 20);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPos, RecoveryRate = 50 });
            var owner = new Owner() { Name = "Ostap" };
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 100, Owner = owner, Position = new Position(9, 23)}};
            var algorithm = new KernytskyiOstapAlgorithm();
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }
    }
}