﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace KernytskyiOstap.RobotChallange
{
    public class KernytskyiOstapAlgorithm : IRobotAlgorithm
    {
        private const int size = 100;
        private const int dist = 3;
        private const int maxEnergy = 1000;
        private bool isUpdated = false;
        public int round = 0;
        private Random rnd = new Random((int)DateTime.Now.Ticks);
        public Dictionary<Position, int> myMap1 { get; }
        private Dictionary<Position, int> myMap2;

        public KernytskyiOstapAlgorithm()
        {
            myMap1 = new Dictionary<Position, int>();
            myMap2 = new Dictionary<Position, int>();
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                {
                    myMap1.Add(new Position(i, j), 0);
                    myMap2.Add(new Position(i, j), -1);
                }
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            round++;
        }

        public void updateOnce(Map map)
        {
            for (int i = 0; i < map.Stations.Count; i++)
            {
                List<Position> positions = new List<Position>();
                for (int j = map.Stations[i].Position.X - dist; j <= map.Stations[i].Position.X + dist; j++)
                    for (int k = map.Stations[i].Position.Y - dist; k <= map.Stations[i].Position.Y + dist; k++)
                        positions.Add(new Position(j, k));
                foreach (var pos in positions)
                    if (myMap1.ContainsKey(pos))
                        myMap1[pos] += 1;
            }

            isUpdated = true;
        }
        class MyEnergyStationsComparer : IEqualityComparer<EnergyStation>
        {
            #region IEqualityComparer<Contact> Members

            public bool Equals(EnergyStation x, EnergyStation y)
            {
                return x.Equals(y);
            }

            public int GetHashCode(EnergyStation obj)
            {
                return obj.GetHashCode();
            }

            #endregion
        }

        private Dictionary<Position, int> updateMap(Map map, IList<Robot.Common.Robot> robots, int index, int robotToMoveIndex)
        {
            var newMap = myMap1.ToDictionary(entry => entry.Key.Copy(), entry => entry.Value);
            List<EnergyStation> usedStations = new List<EnergyStation>();
            for (int i = 0; i < index; i++)
                if (!robots[i].Owner.Equals(robots[robotToMoveIndex].Owner))
                {
                    usedStations.AddRange(map.Stations.Where(station => (Math.Abs(station.Position.X - robots[i].Position.X) <= dist)
                        && (Math.Abs(station.Position.Y - robots[i].Position.Y) <= dist)).ToList());
                    usedStations = usedStations.Distinct(new MyEnergyStationsComparer()).ToList();
                }

            List<Position> tempPos = myMap2.Where(pair => pair.Value >= 0).Select(pos => pos.Key).ToList();
            foreach (var pos in tempPos)
            {
                usedStations.AddRange(map.Stations.Where(station => (Math.Abs(station.Position.X - pos.X) <= dist)
                    && (Math.Abs(station.Position.Y - pos.Y) <= dist)).ToList());
                usedStations = usedStations.Distinct(new MyEnergyStationsComparer()).ToList();
            }
            foreach (var station in usedStations)
            {
                List<Position> positions = new List<Position>();
                for (int j = station.Position.X - dist; j <= station.Position.X + dist; j++)
                    for (int k = station.Position.Y - dist; k <= station.Position.Y + dist; k++)
                        positions.Add(new Position(j, k));
                foreach (var pos in positions)
                    if (newMap.ContainsKey(pos))
                        newMap[pos] -= 1;
            }
            return newMap;
        }

        public string Author
        {
            get { return "Kernytskyi Ostap" + rnd.Next(1000); }
        }

        public string Description
        {
            get { return ""; }
        }

        public int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        public Position FindNearestSpot(Position curPos, Dictionary<Position, int> tempMap)
        {
            Position nearest = null;
            int amount = 0;

            for (int i = 10; i >= 1; i--)
            {
                Position tempNearest = null;
                var positions = tempMap.Where(pair => pair.Value == i && myMap2[pair.Key] == -1).Select(pos => pos.Key).ToList();
                if (positions.Count > 0)
                {
                    tempNearest = positions[0];
                    int minDistance = FindDistance(curPos, positions[0]);
                    for (int j = 1; j < positions.Count; j++)
                        if (FindDistance(curPos, positions[j]) < minDistance)
                        {
                            tempNearest = positions[j];
                            minDistance = FindDistance(curPos, positions[j]);
                        }
                    if (nearest == null)
                    {
                        nearest = tempNearest;
                        amount = i;
                    }
                    else if (minDistance * amount < FindDistance(curPos, nearest) * i)
                    {
                        nearest = tempNearest;
                        amount = i;
                    }
                }
            }
            return nearest;
        }

        private void unmarkStations(int robotToMoveIndex)
        {
            var tempPositions = myMap2.Where(pair => pair.Value == robotToMoveIndex).Select(pos => pos.Key).ToList();
            foreach (var pos in tempPositions)
                myMap2[pos] = -1;
        }

        Boolean canReach(Position start, Position end, int energy, int tempRound)
        {
            var dx = Math.Abs(end.X - start.X);
            var dy = Math.Abs(end.Y - start.Y);
            return tempRound - round - (dx > dy ? dx : dy) >= 0 && dx + dy <= Math.Min(energy, 300);
        }

        private Position findFastWay(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Position end)
        {
            Position start = robots[robotToMoveIndex].Position.Copy();
            var dx = Math.Abs(end.X - start.X);
            var dy = Math.Abs(end.Y - start.Y);
            var energy = Math.Min(robots[robotToMoveIndex].Energy, 300);

            for (int i = 2; true; i++)
            {
                int tempDx, tempDy, tempEnergy;
                if (dx < dy)
                {
                    tempDx = (dx / i == 0 && dx > 0 ? 1 : dx / i);
                    tempDy = (int)Math.Ceiling(1.0 * dy / i);
                }
                else
                {
                    tempDy = (dy / i == 0 && dy > 0 ? 1 : dy / i);
                    tempDx = (int)Math.Ceiling(1.0 * dx / i);
                }
                tempEnergy = (int)Math.Ceiling(1.0 * energy / i);
                if (tempDx * tempDx + tempDy * tempDy <= tempEnergy)
                {
                    start.X = start.X + tempDx * Math.Sign(end.X - start.X);
                    start.Y = start.Y + tempDy * Math.Sign(end.Y - start.Y);
                    bool check = true;
                    foreach (var robot in robots)
                        if (robot.Position.Equals(start))
                        {
                            check = false;
                            break;
                        }
                    if (!check)
                        if (dx > dy)
                            start.X = start.X + (tempDx - 1) * Math.Sign(end.X - start.X);
                        else
                            start.Y = start.X + (tempDy - 1) * Math.Sign(end.Y - start.Y);
                            
                    return start;
                }
            }
        }

        public bool IsValid(Position position)
        {
            return (position.X >= 0) && (position.X < 100) && (position.Y >= 0) &&
                   (position.Y < 100);
        }

        public Position FindFreeCell(Position nearPosition, IList<Robot.Common.Robot> robots, Map map)
        {
            var distance = 1;
            while (distance < 100)
            {
                for (int i = -distance; i <= distance; i++)
                for (int j = -distance; j <= distance; j++)
                {
                    var newPos = new Position(nearPosition.X + i, nearPosition.Y + j);
                    if (!IsValid(newPos))
                        continue;

                    if (!robots.Any(r => r.Position == newPos))
                        return newPos;
                }
                distance++;
            }
            throw new Exception("All cells are filled");
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            if (!isUpdated) updateOnce(map);
            var curRobot = robots[robotToMoveIndex];
            var tempMap = updateMap(map, robots, robots.Count, robotToMoveIndex).ToDictionary(entry => entry.Key.Copy(), entry => entry.Value);
            //var freeCell = map.FindFreeCell(curRobot.Position, robots);
            var freeCell = FindFreeCell(curRobot.Position, robots, map);
            var newPosition = FindNearestSpot(freeCell, tempMap);
            var myRobotsCount = robots.Where(robot => robot.Owner.Equals(curRobot.Owner)).ToList().Count;

            if (newPosition != null && curRobot.Energy >= 250 && myRobotsCount < 100 && canReach(freeCell, newPosition, 200, 45))
            {
                myMap2[newPosition] = robots.Count;
                return new CreateNewRobotCommand() { NewRobotEnergy = 200 };
            }

            unmarkStations(robotToMoveIndex);
            tempMap = updateMap(map, robots, robotToMoveIndex, robotToMoveIndex).ToDictionary(entry => entry.Key.Copy(), entry => entry.Value);
            newPosition = FindNearestSpot(curRobot.Position, tempMap);

            if (curRobot.Position.Equals(newPosition))
            {
                myMap2[curRobot.Position] = robotToMoveIndex;
                return new CollectEnergyCommand();
            }

            if (newPosition != null && canReach(curRobot.Position, newPosition, curRobot.Energy, 46))
            {
                myMap2[newPosition] = robotToMoveIndex;
                if (FindDistance(curRobot.Position, newPosition) <= curRobot.Energy)
                    return new MoveCommand() { NewPosition = newPosition };
                else
                {
                    newPosition = findFastWay(robots, robotToMoveIndex, newPosition);
                    if (myMap1.ContainsKey(newPosition))
                        return new MoveCommand() { NewPosition = newPosition };
                }
            }
            return new CollectEnergyCommand();
        }
    }
}